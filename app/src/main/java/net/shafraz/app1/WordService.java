package net.shafraz.app1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Shafraz on 9/20/2016.
 */
public class WordService extends Service {

    private final IWord.Stub binder = new IWord.Stub() {
        public String process(String string) {

            String[] words = string.split(" ");

            string = "";

            for (int i = 0; i < words.length ; i++) {
                String word = words[i];
                //Checking if contains number
                if(word.matches(".*\\d.*")){
                    string += word + " ";
                } else {
                    //Converting to camel case
                    String ccWord = word.substring(0, 1).toUpperCase() + (word.length() > 1 ?
                            word.substring(1).toLowerCase() : "");
                    string += ccWord + " ";
                }
            }

            return string;
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

}
